jQuery(document).ready(function( $ ){
    new WOW().init();


    $.each($(".menu-item-has-children"), function(i,d){
      $(d).append(`
          <button class="dropdown-toggle">
              <i class="fa fa-chevron-down" aria-hidden="true"></i>
          </button>
      `)
  });
  $(".navigation-wrapper").click(function (e) {
    e.preventDefault();
    
    $(".menu-wrapper").toggleClass("active");
  });
  $("button.close-button").click(function (e) {
    e.preventDefault();
    
    $(".menu-wrapper").removeClass("active");
  });
  // $(".main-navigation ul li a ").click(function (e) {
  //   e.preventDefault();
  //    $(this).addClass('active');
  //    $(this).first().find('.sub-menu').addClass('active');
    
  // });
  $(" .dropdown-toggle").click(function (e) {
    e.preventDefault();
   
  });



$(".col-banner-slider").slick({
  autoplay: true,
  dots: true,
  arrows: false,
  infinite: true,
  fade:true,
   slidesToShow: 1,
  slidesToScroll: 1,
   responsive: [
  {
    breakpoint: 1024,
    settings: {
      dots:true,
      slidesToShow: 1,
      slidesToScroll: 1,
      infinite: true,
 
    }
  },
  {
    breakpoint: 991,
    settings: {
      slidesToShow: 1,
      slidesToScroll: 1,
      vertical:false


    }
  },
  {
    breakpoint: 480,
    settings: {
      autoplay:true,
      vertical:false,
      
      arrows:false,
      slidesToShow: 1,
      slidesToScroll: 1
    }
  }
]
});
$(".event-slider").slick({
  autoplay: true,
  dots: true,
  arrows: false,
  infinite: true,
  
   slidesToShow: 2,
  slidesToScroll: 1,
   responsive: [
  {
    breakpoint: 1024,
    settings: {
      dots:true,
      slidesToShow: 3,
      slidesToScroll: 1,
      infinite: true,
 
    }
  },
  {
    breakpoint: 991,
    settings: {
      slidesToShow: 1,
      slidesToScroll: 1,
      vertical:false


    }
  },
  {
    breakpoint: 480,
    settings: {
      autoplay:true,
      vertical:false,
      
      arrows:false,
      slidesToShow: 1,
      slidesToScroll: 1
    }
  }
]
});
//slider-end

$('.nav-menu').superfish({
  animation: {
      opacity: 'show'
  },
  speed: 400
});
$(window).scroll(function() {
  if ($(this).scrollTop() > 50) {
      $('#header').addClass('header-scrolled');
      $('#mobile-nav-toggle').addClass('scrolled');
  } else {
      $('#header').removeClass('header-scrolled');
      $('#mobile-nav-toggle').removeClass('scrolled');
  }
});
//------- Mobile Nav  js --------//  

if ($('#nav-menu-container').length) {
  var $mobile_nav = $('#nav-menu-container').clone().prop({
      id: 'mobile-nav'
  });
  $mobile_nav.find('> ul').attr({
      'class': '',
      'id': ''
  });
  $('body').append($mobile_nav);
  $('body').prepend('<button type="button" id="mobile-nav-toggle"><i class="fa fa-bars"></i></button>');
  $('body').append('<div id="mobile-body-overly"></div>');
  $('#mobile-nav').find('.menu-has-children').prepend('<i class="fa fa-angle-down"></i>');

  $(document).on('click', '.menu-has-children i', function(e) {
      $(this).next().toggleClass('menu-item-active');
      $(this).nextAll('ul').eq(0).slideToggle();
      $(this).toggleClass("fa-angle-up fa-angle-down");
  });

  $(document).on('click', '#mobile-nav-toggle', function(e) {
      $('body').toggleClass('mobile-nav-active');
      $('#mobile-nav-toggle i').toggleClass('fa-times fa-bars');
      $('#mobile-body-overly').toggle();
  });

      $(document).on('click', function(e) {
      var container = $("#mobile-nav, #mobile-nav-toggle");
      if (!container.is(e.target) && container.has(e.target).length === 0) {
          if ($('body').hasClass('mobile-nav-active')) {
              $('body').removeClass('mobile-nav-active');
              $('#mobile-nav-toggle i').toggleClass('fa-times fa-bars');
              $('#mobile-body-overly').fadeOut();
          }
      }
  });
} else if ($("#mobile-nav, #mobile-nav-toggle").length) {
  $("#mobile-nav, #mobile-nav-toggle").hide();
}

//------- Smooth Scroll  js --------//  

$('.nav-menu a, #mobile-nav a, .scrollto').on('click', function() {
  if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
      var target = $(this.hash);
      if (target.length) {
          var top_space = 0;

          if ($('#header').length) {
              top_space = $('#header').outerHeight();

              if (!$('#header').hasClass('header-fixed')) {
                  top_space = top_space;
              }
          }

          $('html, body').animate({
              scrollTop: target.offset().top - top_space
          }, 1500, 'easeInOutExpo');

          if ($(this).parents('.nav-menu').length) {
              $('.nav-menu .menu-active').removeClass('menu-active');
              $(this).closest('li').addClass('menu-active');
          }

          if ($('body').hasClass('mobile-nav-active')) {
              $('body').removeClass('mobile-nav-active');
              $('#mobile-nav-toggle i').toggleClass('fa-times fa-bars');
              $('#mobile-body-overly').fadeOut();
          }
          return false;
      }
  }
});

$(document).ready(function() {

  $('html, body').hide();

  if (window.location.hash) {

      setTimeout(function() {

          $('html, body').scrollTop(0).show();

          $('html, body').animate({

              scrollTop: $(window.location.hash).offset().top - 108

          }, 1000)

      }, 0);

  } else {

      $('html, body').show();

  }

});
/*------------------------------------------------
               FANCY NAVIGATION
        ------------------------------------------------*/
        $("[data-fancybox]").fancybox();
//------- Superfist nav menu  js --------//  

});//end of jquery
